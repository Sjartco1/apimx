//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser');

app.use(bodyparser.json());

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/acruz/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLabRaiz;

var urlClientes = "https://api.mlab.com/api/1/databases/acruz/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientes)

var urlUsuarios = "https://api.mlab.com/api/1/databases/acruz/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMLab = requestjson.createClient(urlUsuarios)

var urlCuentas = "https://api.mlab.com/api/1/databases/acruz/collections/Cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var cuentasMLab = requestjson.createClient(urlCuentas)

app.listen(port);

var bodyparser = require('body-parser');
app.use(bodyparser.json());

var movimientosJSON = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function (req,res){
  res.send("hemos recibido su peticion de POST cambiada");
})

app.put('/', function (req,res){
  res.send("hemos recibido su peticion de PUT");
})

app.delete('/', function (req,res){
  res.send("hemos recibido su peticion de Delete");
})

app.get('/Clientes/:idcliente', function (req,res){
  res.send("Aqui tiene al cliente numero "+ req.params.idcliente);
})

app.get('/v1/Movimientos', function (req,res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function (req,res){
  res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id', function (req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id - 1]);
})

app.get('/v2/MovimientosQuery', function (req,res){
  console.log(req.query);
  res.send("Se recibio el Query ");
})

app.post('/v2/Movimientos', function (req, res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de Alta")
})

app.get('/Clientes', function(req, res){
  clienteMLab.get('',function(err, resM, body){
    if(err){
        console.log(body)
    }
    else{
      res.send(body);
    }
  })
})

app.post('/Clientes',function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body){
      res.send(body);
  })
})

app.post('/Login', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
  console.log(query)

  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        res.status(200).send('Usuario logado')
      } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

app.get('/Registrar', function(req, res){
  usuarioMLab.get('',function(err, resM, body){
    if(err){
        console.log(body)
    }
    else{
      res.send(body);
    }
  })
})
app.post('/Registrar',function(req, res){
  usuarioMLab.post('', req.body, function(err, resM, body){
      res.send(body);
  })
})

app.get('/RegistrarMovimiento', function(req, res){
  cuentasMLab.get('',function(err, resM, body){
    if(err){
        console.log(body)
    }
    else{
      res.send(body);
    }
  })
})
app.post('/RegistrarMovimiento',function(req, res){
  cuentasMLab.post('', req.body, function(err, resM, body){
      res.send(body);
  })
})

/*
app.post('/Registrar',function(req, res){
  console.log("Petición recibida: "+body + " y usuarioMLab: "+usuarioMLab);
  usuarioMLab.post('', req.body, function(err, resM, body){
      res.send(body);
  })
}
*/
/*app.post('/Usuarios', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var idCliente = req.body.idCliente
  var nombre = req.body.nombre
  var apellido = req.body.apellido
  var email = req.body.email
  var password = req.body.password
  var body = '{"idCliente":"'+ idCliente +'","nombre":"'+ nombre +'","apellido":"'+ apellido +'","email":"'+ email +'","password":"' +password + '"}'
  console.log(cuerpo)
  //var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  var urlMLab = urlMlabRaiz + "/Usuarios?" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)
  clienteMlabRaiz.get('', function(err, resM, body) {
  //clienteMlabRaiz.post('', req.body, function(err, resM, body){
    //res.send(body);
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        res.status(200).send('Usuario logado')
      } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})*/
